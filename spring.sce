//Izabela Moraczewska
//Technical Physics
//Following code was written in Scilab

clear

r = 0.002 // wire radius [m]
R = 2* 0.0254 //  spring radius [m]
m = 0.01 // mass [kg]
tau = 2
G = 79.3e9
beta = 1/(2*tau)

g = 9.81

F = m*g // gravitational force 
alpha = F/m

//calculating
for N = 2:50


k = (G*r^4)/(4*N*R^3)
omega_0 = sqrt(k/m)


for i = 1:1000
    omega = i/2
    A(i) = alpha/(sqrt((omega_0^2-omega^2)^2+(4*beta^2*omega^2)))
    o(i) = omega

end

Max(N)=max(A)
x(N) = N

//creating tables for results of calculating
subplot(211)
plot(o,A)
//creating titles
xtitle(" ","Frequency[Hz]","Amplitude[m]")
subplot(212)
plot(x,Max)
xtitle(" ","MaxAmplitude[m]","Number of coils")
end

xs2pdf(0, 'resonance.pdf')